# Pertanyaan Lab 2
### Muhammad Agil Ghifari (2006595835)

1. Perbedaan antara JSON dengan XML:
- JSON menggunakan notasi object dalam javascript, XML menggunakan tag seperti html
- JSON dapat membuat array, sementara XML tidak
- XML lebih aman dibandingkan JSON
- XML dapat diberikan komentar, sementara JSON tidak

2. Perbedaan HTML dan XML
- HTML bersifat statik, sementara XML dinamik
- HTML adalah bahasa markup, XML dapat digunakan untuk mendefinisikan markup language
- HTML bukan case sensitive, XML sebaliknya
- HTML tags sudah predefined, sementara XML dibuat sendiri
- HTML memotong white space berlebih, sementara XML tidak
- HTML berfungsi untuk menampilkan data, sementara XML untuk deskripsi data dan penyimpanan