from django.urls import path, include
from .views import index, xml, json

# TODO Add friends path using friend_list Views
urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]
