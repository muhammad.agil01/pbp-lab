from django.db import models

# Create your models here.


class Note(models.Model):
    frm = models.CharField(max_length=30)
    to = models.CharField(max_length=30)
    title = models.CharField(max_length=40)
    message = models.CharField(max_length=120)
