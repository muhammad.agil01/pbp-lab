from django.urls import path, include
from .views import index, get_note

# TODO Add friends path using friend_list Views
urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', get_note)
]
