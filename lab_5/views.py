from django.shortcuts import render
from lab_2.models import Note
from django.core import serializers
from django.http.response import HttpResponse

# Create your views here.


def index(request):
    notes = Note.objects.all().values()
    ids = Note.objects.all().values('id')
    response = {'notes': notes, 'ids': ids}
    return render(request, 'lab5_index.html', response)


def get_note(request, id):
    response = {}
    #response["note"] = Note.objects.get(id=id)
    # return render(request, 'note_modal.html', response)
    data = serializers.serialize('json', [Note.objects.get(id=id)])
    return HttpResponse(data, content_type="application/json")
