/// Flutter code sample for Scaffold

// This example shows a [Scaffold] with a [body] and [FloatingActionButton].
// The [body] is a [Text] placed in a [Center] in order to center the text
// within the [Scaffold]. The [FloatingActionButton] is connected to a
// callback that increments a counter.
//
// ![The Scaffold has a white background with a blue AppBar at the top. A blue FloatingActionButton is positioned at the bottom right corner of the Scaffold.](https://flutter.github.io/assets-for-api-docs/assets/material/scaffold.png)

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
            'Temeninisoman',
          style: TextStyle(fontWeight: FontWeight.bold, color: Color(0XFFEFEFEF), fontFamily: "serif"),
        ),
        backgroundColor: const Color(0xFF454545),
        actions: <Widget> [
          Container(
            child: Text("LOGIN"),
            margin: EdgeInsets.fromLTRB(30.0, 20.0, 20.0, 20.0),
            color: Colors.pinkAccent,
          ),
        ],
      ),
      body: ListView(
        children: <Widget> [
          Container(
            padding: const EdgeInsets.fromLTRB(30.0, 80.0, 10.0, 10.0),
            height: 160,
            color: const Color(0xFF343434),
            child: const Text('Info Bed Request',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 40.0),
              ),
          ),
          Container(
            color: const Color(0xFFe9e9e9),
            child: Container(
              color: const Color(0xFFefefef),
              margin: const EdgeInsets.all(50.0),
              child: Table(
                children: const <TableRow> [
                  TableRow(
                    children: <Widget> [
                      Text("Nama Rumah Sakit",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54, fontSize: 30.0),),
                      Text("Kapasitas",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54, fontSize: 30.0),)],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
