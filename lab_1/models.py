from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here
# python manage.py makemigrations  || migrate


class Friend(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model
    npm = models.BigIntegerField()
    dob = models.DateField()
