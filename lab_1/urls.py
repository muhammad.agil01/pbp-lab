from django.urls import path, include
from .views import index, friend_list

# TODO Add friends path using friend_list Views
urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name='friends')
]
