from django import forms
from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']

    error_messages = {
        'required': 'Please Type'
    }
    input_attrs = {
        'type': 'text',
        'placeholder': 'Nama Kamu'
    }
    nama = forms.CharField(label='', required=False,
                           max_length=30, error_messages=error_messages)
    npm = forms.DecimalField(label='', required=False)
    dob = forms.DateField(label='', required=False)
