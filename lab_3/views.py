from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


@login_required(login_url="/admin/login/")
def add_friend(request):
    context = {}

    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)

    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        # <-- habis deadline ditambahinnya
        return HttpResponseRedirect('/lab-3')

    context['form'] = form
    return render(request, "lab3_form.html", context)
