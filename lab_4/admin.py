from django.contrib import admin
from .models import AdvanceNote

# Register your models here.
admin.site.register(AdvanceNote)
