from django.urls import path, include
from .views import index, add_note

# TODO Add friends path using friend_list Views
urlpatterns = [
    path('', index, name='index'),
    path('add', add_note, name='add_note')
]
