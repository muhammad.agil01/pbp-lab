from django import forms
from lab_4.models import AdvanceNote


class NoteForm(forms.ModelForm):
    class Meta:
        model = AdvanceNote
        fields = ['frm', 'to', 'title', 'message']

    # error_messages = {
    #     'required': 'Please Type'
    # }
    # input_attrs = {
    #     'type': 'text',
    #     'placeholder': 'Masukkan Sesuatu di sini'
    # }
    # frm = forms.CharField(label='frm', required=True,
    #                       max_length=30, widget=forms.TextInput(attrs=input_attrs))
    # to = forms.CharField(label='to', required=True, max_length=30,
    #                      widget=forms.TextInput(attrs=input_attrs))
    # title = forms.CharField(label='title', required=True,
    #                         max_length=40, widget=forms.TextInput(attrs=input_attrs))
    # message = forms.CharField(label='message', required=True,
    #                           max_length=120, widget=forms.TextInput(attrs=input_attrs))
