from django.http import HttpResponseRedirect
from .forms import NoteForm
from django.shortcuts import render
from lab_4.models import AdvanceNote

# Create your views here.


def index(request):
    notes = AdvanceNote.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-4')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})
