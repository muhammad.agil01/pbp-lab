from django.db import models
from lab_2.models import Note
from datetime import datetime
from django.utils import timezone

# Create your models here.


class AdvanceNote(Note):
    date = models.DateTimeField(default=datetime.now())
