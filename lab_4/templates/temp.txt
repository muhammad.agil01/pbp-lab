<!DOCTYPE html>
{% load static %}
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>My Note</title>
    <link rel="stylesheet" href="{% static 'css/style.css' %}">
  </head>

  <body>

    <div class="titlediv">
      <h1>Welcome to my notebook!</h1>
    </div>
    
    <div class="topnav">
      <a class="active" href="">Daftar Note</a>
      <a href="add">Masukkan Note</a>
    </div>

    <div>
    <div class="notecontainer">
      {% if notes %}
      {% for note in notes %}
      <div class="note">
        <h3>{{ note.title }}</h3>
        <h4>From: {{ note.frm }}</h4>
        <h4>To: {{ note.to }}</h4>
        <p> {{ note.message }} </p>
        <p class="date"> {{ note.date }} </p>
      </div>
      {% endfor %}
      {% endif %}
  </div>
  </div>
  </body>
</html>
